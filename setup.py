import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="radiotap", # Replace with your own username
    version="0.0.1",
    author="Bob Copeland",
    author_email="author@example.com",
    description="Packaging of the python-radiotap module",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/radiotap/python-radiotap",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: ISC License (ISCL)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
)
